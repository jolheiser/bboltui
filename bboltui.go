package bboltui

import (
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"go.etcd.io/bbolt"
)

type BucketInfo struct {
	Sub     []string
	Seq     uint64
	Buckets []string
	Data    map[string]string

	IsBucket bool
}

func (b BucketInfo) SubPath(idx int) string {
	idx++
	if idx <= 0 || idx > len(b.Sub) {
		idx = len(b.Sub)
	}
	return strings.Join(b.Sub[:idx], "/")
}

// Handler sets up a new bboltui handler
func Handler(db *bbolt.DB) http.Handler {
	mux := chi.NewMux()
	mux.Use(middleware.Recoverer)
	mux.Use(middleware.Timeout(time.Minute))
	mux.Use(middleware.StripSlashes)
	mux.Get("/*", info(db))
	mux.Route("/_", func(r chi.Router) {
		r.Get("/backup", backup(db))
	})
	return mux
}

func info(db *bbolt.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		buckets := strings.Split(chi.URLParam(r, "*"), "/")
		bucketInfo, err := Bucket(db, buckets)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err := templates.Lookup("info.tmpl").Execute(w, bucketInfo); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

func backup(db *bbolt.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := db.View(func(tx *bbolt.Tx) error {
			w.Header().Set("Content-Type", "application/octet-stream")
			w.Header().Set("Content-Disposition", `attachment; filename="bbolt.db"`)
			w.Header().Set("Content-Length", strconv.Itoa(int(tx.Size())))
			_, err := tx.WriteTo(w)
			return err
		}); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

type bc interface {
	Bucket(name []byte) *bbolt.Bucket
	Cursor() *bbolt.Cursor
}

func Bucket(db *bbolt.DB, buckets []string) (BucketInfo, error) {
	info := BucketInfo{
		Sub:     buckets,
		Seq:     0,
		Buckets: make([]string, 0),
		Data:    make(map[string]string),
	}

	if err := db.View(func(tx *bbolt.Tx) error {
		var b bc = tx
		for _, bucket := range buckets {
			if strings.TrimSpace(bucket) == "" {
				break
			}
			b = b.Bucket([]byte(bucket))
			if b == (*bbolt.Bucket)(nil) {
				return errors.New("bucket does not exist")
			}
			info.IsBucket = true
		}

		cur := b.Cursor()
		for key, val := cur.First(); key != nil; key, val = cur.Next() {
			// Bucket
			if val == nil {
				info.Buckets = append(info.Buckets, string(key))
				continue
			}
			// Data
			info.Data[string(key)] = string(val)
		}
		// Sequence
		if bucket, ok := b.(*bbolt.Bucket); ok {
			info.Seq = bucket.Sequence()
		}

		return nil
	}); err != nil {
		return info, err
	}

	return info, nil
}
