module go.jolheiser.com/bboltui

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.1
	go.etcd.io/bbolt v1.3.5
	go.jolheiser.com/overlay v0.0.2
)
