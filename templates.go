package bboltui

import (
	"embed"
	"html/template"
	"io/fs"
	"os"

	"go.jolheiser.com/overlay"
)

var (
	//go:embed templates
	templateEmbeds embed.FS
	templateFS     fs.FS = templateEmbeds
	templates      *template.Template
)

func init() {
	tfs, err := overlay.New(os.Getenv("BBOLTUI_TEMPLATES"), templateEmbeds, overlay.WithSub("templates"))
	if err != nil {
		return
	}
	templateFS = tfs

	templates = template.Must(template.New("").ParseFS(tfs, "*"))
}
