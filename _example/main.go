package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"go.jolheiser.com/bboltui"

	"go.etcd.io/bbolt"
)

func main() {
	db, err := bbolt.Open("example.db", os.ModePerm, bbolt.DefaultOptions)
	if err != nil {
		panic(err)
	}
	defer os.Remove("example.db")
	defer db.Close()
	if err = exampleData(db); err != nil {
		panic(err)
	}

	fmt.Println("http://localhost:8000")
	go func() {
		if err = http.ListenAndServe(":8000", bboltui.Handler(db)); err != nil {
			panic(err)
		}
	}()

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt, os.Kill)
	<-ch
	fmt.Println("Cleaning up...")
}

func exampleData(db *bbolt.DB) error {
	return db.Update(func(tx *bbolt.Tx) error {
		ba, err := tx.CreateBucket([]byte("bucketA"))
		if err != nil {
			return err
		}
		if err = ba.Put([]byte("a"), []byte("Item A")); err != nil {
			return err
		}

		bb, err := tx.CreateBucket([]byte("bucketB"))
		if err != nil {
			return err
		}
		bb = bb

		bc, err := tx.CreateBucket([]byte("bucketC"))
		if err != nil {
			return err
		}
		if err := bc.SetSequence(1234); err != nil {
			return err
		}
		bc = bc

		bd, err := ba.CreateBucket([]byte("bucketD"))
		if err != nil {
			return err
		}
		bd = bd

		return nil
	})
}
